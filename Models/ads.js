const Users = require('./users');

module.exports = (sequelize, DataTypes) => {
    const Ads = sequelize.define(
        'Ads',
        {
            title: {
                type: DataTypes.STRING,
                unique: true,
                allowNull: false,
                validate: {
                    max: 100,
                    min: 10,
                    notNull: { args: true, msg: 'You must enter a title' },
                },
            },
            body: {
                type: DataTypes.TEXT,
                allowNull: false,
                validate: {
                    notNull: { args: true, msg: 'You must enter a body' },
                },
            },
            price: {
                type: DataTypes.INTEGER,
                unique: false,
                allowNull: false,
                validate: {
                    notNull: { args: true, msg: 'You must enter a price' },
                    isInt: true,
                },
            },
            phoneNumber: {
                type: DataTypes.STRING,
                allowNull: false,
                validate: {
                    notNull: {
                        args: true,
                        msg: 'You must enter a phone number',
                    },
                },
            },
            filePath: {
                type: DataTypes.STRING,
                allowNull: false,
                validate: {
                    max: 100,
                    min: 10,
                    notNull: { args: true, msg: 'You must upload a file' },
                },
            },
            location: {
                type: DataTypes.TEXT,
                allowNull: false,
                get() {
                    const rawValue = this.getDataValue('location');
                    return rawValue ? JSON.parse(rawValue) : null;
                },
            },
            isOnline: {
                type: DataTypes.BOOLEAN,
                allowNull: false,
                defaultValue: false,
            },
            lbcId: {
                type: DataTypes.STRING,
            },
            user_id: {
                type: DataTypes.INTEGER,
                allowNull: false,
                references: { model: 'users', key: 'id' },
            },
        },
        { tableName: 'ads', omitNull: true }
    );

    Ads.associate = (models) => {
        Ads.belongsTo(models.Users);
    };

    return Ads;
};

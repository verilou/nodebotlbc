module.exports = (sequelize, DataTypes) => {
    const CodesPostaux = sequelize.define(
        'CodesPostaux',
        {
            code_commune_insee: {
                type: DataTypes.STRING(5),
                allowNull: false,
            },
            nom_commune: {
                type: DataTypes.STRING(38),
                allowNull: false,
            },
            code_postal: {
                type: DataTypes.INTEGER,
                allowNull: false,
            },
            ligne_5: {
                type: DataTypes.STRING(33),
                allowNull: false,
            },
            libelle: {
                type: DataTypes.STRING(32),
                allowNull: false,
            },
        },
        { tableName: 'codes_postaux', timestamps: false }
    );

    return CodesPostaux;
};

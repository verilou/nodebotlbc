module.exports = (sequelize, DataTypes) => {
    const Users = sequelize.define(
        'Users',
        {
            email: {
                type: DataTypes.STRING,
                unique: true,
                allowNull: false,
                validate: {
                    max: 100,
                    isEmail: true, // checks for email format (foo@bar.com)
                    notNull: { args: true, msg: 'You must enter a email' }, // don't allow empty strings
                },
            },
            refresh_token: {
                type: DataTypes.STRING,
                allowNull: true,
            },
            access_token: {
                type: DataTypes.STRING,
                allowNull: true,
            },
            state: {
                type: DataTypes.STRING,
                allowNull: false,
            },
        },
        { tableName: 'users' }
    );

    Users.associate = (models) => {
        Users.hasMany(models.Ads, {
            foreignKey: 'user_id',
            onDelete: 'SET NULL',
            onUpdate: 'CASCADE',
        });
    };

    return Users;
};

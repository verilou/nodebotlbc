const { Op } = require('sequelize');
const fs = require('fs');
const { Ads, CodesPostaux, Users } = require('../models');
const UploadAdLbc = require('../services/Lbc/UploadAdLbc');
const multer = require('../middlewares/multer');

const upload = multer.single('file');

module.exports = {
    create: async (ctx, next) => {
        await upload(ctx, next);
        const { body } = ctx.request;
        body.filePath = ctx.file.filename;
        body.user_id = ctx.state.user.id;
        const created = await Ads.create(body);
        ctx.body = created;
    },

    del: async (ctx) => {
        const { id } = ctx.params;

        if (id) {
            const ads = await Ads.findByPk(id);
            fs.unlinkSync(ads.filePath);
            await Ads.destroy({
                where: {
                    id,
                },
            });
            ctx.body = ads;
        }
    },

    update: async (ctx, next) => {
        const {
            params: { id },
            request: { body },
        } = ctx;
        const updated = await Ads.update(body, {
            where: { id },
        });
        ctx.body = updated;
    },

    list: async (ctx) => {
        const AllAds = await Ads.findAll({
            where: {
                user_id: ctx.state.user.id,
            },
        });
        ctx.body = AllAds;
    },

    autocomplete: async (ctx) => {
        const { string } = ctx.query;
        ctx.body = await CodesPostaux.findAll({
            where: {
                nom_commune: {
                    [Op.substring]: string.toUpperCase(),
                },
            },
        });
    },

    details: async (ctx) => {
        const { id } = ctx.params;
        if (id) {
            const ad = await Ads.findByPk(id);
            ctx.body = ad;
        }
    },

    recreate: async (ctx, next) => {
        const adId = ctx.params.id;
        const user = await Users.findByPk(ctx.state.user.id);
        const ad = await Ads.findByPk(adId);
        const lbcId = await UploadAdLbc({ user, ad });
        console.log(lbcId);
        // await Ads.update(lbcId, {
        // 	where: {
        // 		id: adId,
        // 	},
        // });

        ctx.body = json;
    },
};

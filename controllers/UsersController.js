const jwt = require('jsonwebtoken');

const { Users } = require('../models');
const AuthLbc = require('../services/Lbc/AuthLbc');

module.exports = {
    register: async (ctx, next) => {
        const body = ctx.request.body;
        const authLbcResponse = await AuthLbc(body);
        const newUser = await Users.findOrCreate({
            where: {
                email: body.email,
            },
            defaults: {
                email: body.email,
                refresh_token: authLbcResponse.refresh_token,
                access_token: authLbcResponse.access_token,
                state: authLbcResponse.uuid,
            },
        });
        const token = jwt.sign(
            { id: newUser.id },
            'its our secret key from toulouse'
        );
        ctx.body = { newUser, token };
    },

    login: async (ctx, next) => {
        const body = ctx.request.body;
        const user = await Users.findOne({ where: { email: body.email } });
        if (user) {
            let authLbcResponse = await AuthLbc(body);
            user.access_token = authLbcResponse.access_token;
            user.state = authLbcResponse.uuid;
            user.refresh_token = authLbcResponse.refresh_token;
            await user.save();
            const token = jwt.sign(
                { id: user.id },
                'its our secret key from toulouse'
            );
            ctx.body = { user, token };
        } else {
            ctx.throw(403, 'User not found');
        }
    },

    del: async (ctx, next) => {
        const id = ctx.params.id;
        if (id) {
            const ads = await Users.destroy({
                where: {
                    id: id,
                },
            });
            ctx.body = ads;
        }
    },

    update: async (ctx, next) => {
        const {
            params: { id },
            request: { body },
        } = ctx;
        const updated = await Users.update(body, {
            where: { id: id },
        });
        ctx.body = updated;
    },

    list: async (ctx, next) => {
        let AllAds = await Users.findAll();
        ctx.body = AllAds;
    },

    details: async (ctx, next) => {
        const id = ctx.params.id;
        if (id) {
            const ad = await Users.findByPk(id);
            ctx.body = ad;
        }
    },
};

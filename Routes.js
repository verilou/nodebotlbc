const Router = require('@koa/router');
const ads = require('./Controllers/AdsController.js');
const users = require('./Controllers/UsersController.js');
const router = new Router();
const jwt = require('koa-jwt');

router.use('/ads', jwt({ secret: 'its our secret key from toulouse' }));

router
    .put('/ads', ads.create)
    .get('/ads', ads.list)
    .get('/ads/recreate/:id', ads.recreate)
    .get('/ads/:id', ads.details)
    .patch('/ads/:id', ads.update)
    .del('/ads/:id', ads.del)
    .get('/autocomplete', ads.autocomplete)
    .put('/auth', users.register)
    .post('/auth', users.login)
    .get('/users', users.list)
    .get('/users/:id', users.details)
    .patch('/users/:id', users.update)
    .del('/users/:id', users.del);

module.exports = router;

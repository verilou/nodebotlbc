'use strict';

module.exports = {
    up: async (queryInterface, Sequelize) => {
        queryInterface.addColumn('ads', 'user_id', {
            type: Sequelize.INTEGER,
            references: {
                model: 'users',
                key: 'id',
            },
            onUpdate: 'cascade',
            onDelete: 'cascade',
        });
    },

    down: async (queryInterface, Sequelize) => {
        queryInterface.removeColumn('ads', 'user_id');
    },
};

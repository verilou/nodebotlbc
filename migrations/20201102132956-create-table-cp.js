'use strict';

module.exports = {
    up: async (queryInterface, Sequelize) => {
        /**
         * Add altering commands here.
         *
         * Example:
         * await queryInterface.createTable('users', { id: Sequelize.INTEGER });
         */
        /**
         * Add altering commands here.
         *
         * Example:
         * await queryInterface.createTable('users', { id: Sequelize.INTEGER });
         */
        await queryInterface.createTable('codes_postaux', {
            id: {
                primaryKey: true,
                autoIncrement: true,
                type: Sequelize.INTEGER,
            },
            code_commune_insee: {
                type: Sequelize.STRING(5),
                allowNull: false,
            },
            nom_commune: {
                type: Sequelize.STRING(38),
                allowNull: false,
            },
            code_postal: {
                type: Sequelize.INTEGER,
                unique: false,
                allowNull: false,
            },
            ligne_5: Sequelize.STRING(33),
            libelle: Sequelize.STRING(32),
        });
    },

    down: async (queryInterface, Sequelize) => {
        /**
         * Add reverting commands here.
         *
         * Example:
         * await queryInterface.dropTable('users');
         */
        await queryInterface.dropTable('codes_postaux');
    },
};

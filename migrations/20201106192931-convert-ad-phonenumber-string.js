'use strict';

module.exports = {
    up: (queryInterface, Sequelize) => {
        return Promise.all([
            queryInterface.changeColumn('ads', 'phoneNumber', {
                type: Sequelize.STRING,
            }),
        ]);
    },

    down: (queryInterface, Sequelize) => {
        return Promise.all([
            queryInterface.changeColumn('ads', 'phoneNumber', {
                type: Sequelize.INTERGER,
            }),
        ]);
    },
};

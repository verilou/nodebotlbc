'use strict';

module.exports = {
    up: async (queryInterface, Sequelize) => {
        /**
         * Add altering commands here.
         *
         * Example:
         * await queryInterface.createTable('users', { id: Sequelize.INTEGER });
         */
        return queryInterface.addColumn('ads', 'location', {
            type: Sequelize.TEXT,
            allowNull: false,
            defaultValue: `{"location":{"address":"","district":"","city":"Créteil","label":"Créteil (94000)","lat":48.77745,"lng":2.45519,"zipcode":"94000","geo_source":"city","geo_provider":"here","region":"12","region_label":"Ile-de-France","department":"94","dpt_label":"Val-de-Marne","country":"FR"}}`,
        });
    },

    down: async (queryInterface, Sequelize) => {
        /**
         * Add reverting commands here.
         *
         * Example:
         * await queryInterface.dropTable('users');
         */
        return queryInterface.removeColumn('ads', 'location');
    },
};

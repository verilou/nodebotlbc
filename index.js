const Koa = require('koa');
const router = require('./Routes');
const bodyParser = require('koa-bodyparser');
const cors = require('@koa/cors');
const serve = require('koa-static');
const path = require('path');

const errorHandler = require('./Middlewares/errorHandler');
const app = new Koa();

app.use(errorHandler)
    .use(bodyParser())
    .use(cors())
    .use(serve(path.join('public', 'ads')))
    .use(router.routes())
    .use(router.allowedMethods());

app.listen(5000);

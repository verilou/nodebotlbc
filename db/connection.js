const { Sequelize, DataTypes } = require('sequelize');
const path = require('path');
const { readdirSync } = require('fs');

const db = new Sequelize('postgres://postgres:root@localhost:5432/BotLbc');
const modelsPath = path.join(__dirname, '..', 'Models');
const skipFiles = ['index.js'];

readdirSync(modelsPath)
    .filter((file) => {
        return file.indexOf('.') !== 0 && !skipFiles.includes(file);
    })
    .forEach((file) => {
        const model = require(path.join(modelsPath, file))(db, DataTypes);
        db[model.name] = model;
    });

module.exports = db;

module.exports = parseCookie = (str) => str.split(';').map((v) => v.split('='));

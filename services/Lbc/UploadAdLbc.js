const axios = require('axios');
const fs = require('fs');
const path = require('path');
const FormData = require('form-data');

module.exports = async ({ user, ad }) => {
    axios.defaults.withCredentials = true;
    const {
        location: {
            hereResult: {
                address: { city, state, county },
                position: { lat, lng },
            },
            cp,
        },
        filePath,
    } = ad;

    const upload = async () => {
        const adPath = path.join(
            __dirname,
            '..',
            '..',
            'public',
            'ads',
            filePath
        );
        let newFile = fs.createReadStream(adPath);
        let photo = [];
        try {
            const form_data = new FormData();
            form_data.append('file', newFile);
            const uploadedPhoto = await axios.post(
                'https://api.leboncoin.fr/api/pintad/v1/public/upload/image',
                form_data,
                {
                    headers: {
                        Authorization: 'Bearer ' + user.access_token,
                        ...form_data.getHeaders(),
                    },
                }
            );
            photo[0] = {
                name: uploadedPhoto.data.filename,
                url: uploadedPhoto.data.url,
            };
        } catch (error) {
            console.log(error);
        }
        const json = {
            ad_type: 'sell',
            body: ad.body,
            email: user.email,
            attributes: {},
            no_salemen: true,
            phone: ad.phoneNumber.toString(),
            price: ad.price.toString(),
            phone_hidden: false,
            subject: ad.title,
            category_id: '34',
            location: {
                address: '',
                district: '',
                city,
                label: city + ' (' + cp + ')',
                lat,
                lng,
                zipcode: cp.toString(),
                geo_source: 'city',
                geo_provider: 'here',
                region: '12',
                region_label: state,
                department: cp.toString().slice(0, -3),
                dpt_label: county,
                country: 'FR',
            },
            pricing_id: '61433ca556bba5d086a230854d21eaf4',
            images: photo,
        };
        try {
            const result = await axios.post(
                'https://api.leboncoin.fr/api/adsubmit/v1/validators',
                json,
                {
                    headers: {
                        Authorization: 'Bearer ' + user.access_token,
                    },
                }
            );
            console.log(result.status, 'here');
        } catch (error) {
            console.log(error.response.data);
        }
        let responseResult = '';
        try {
            const result = await axios.post(
                'https://api.leboncoin.fr/api/adsubmit/v1/classifieds',
                json,
                {
                    headers: {
                        Authorization: 'Bearer ' + user.access_token,
                    },
                }
            );
            responseResult = result;
            console.log(result);
        } catch (error) {
            console.log(error);
        }
        return responseResult;
    };

    // const suprrAd = async () => {
    //     ad.lbcId
    // };

    // if (ad.isOnline) {
    // 	await suprrAd();
    // }

    const lbcId = await upload();
    return lbcId;
};

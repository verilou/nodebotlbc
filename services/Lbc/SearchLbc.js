const axios = require('axios');
axios.defaults.withCredentials = true;
module.exports = SearchLbc = async (token) => {
    const config = {
        headers: { Authorization: `Bearer ${token}` },
    };
    try {
        const result = await axios.get(
            'https://api.leboncoin.fr/api/accounts/v1/accounts/me/personaldata',
            config,
            { withCredentials: true }
        );
    } catch (error) {
        console.log(error.response.headers['set-cookie']);
        // console.log(error);
    }

    const {
        data: { ads },
    } = await axios.get(
        'https://api.leboncoin.fr/api/dashboard/v1/search',
        {
            context: 'default',
            filters: { owner: { store_id: storeId } },
            include_inactive: true,
            limit: 30,
            offset: 0,
            pivot: null,
            sort_by: 'date',
            sort_order: 'desc',
        },
        config
    );
    return ads;
};

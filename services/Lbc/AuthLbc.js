const { v4: uuidv4 } = require('uuid');
const axios = require('axios');
const cookiesParser = require('../CookiesParser');
axios.defaults.withCredentials = true;

module.exports = async (body) => {
    const uuid = uuidv4();
    const auth_payload = {
        email: body.email,
        password: body.password,
        client_id: 'lbc-front-web',
        from_to: 'https://www.leboncoin.fr/',
        redirect_uri:
            'https://auth.leboncoin.fr/api/authorizer/v2/authorize?client_id=lbc-front-web&redirect_uri=https%3A%2F%2Fwww.leboncoin.fr%2Foauth2callbacknext%2F&scope=*%20offline&response_type=code&state=' +
            uuid +
            '&prompt=none&force_redirect=true',
    };
    const response = await axios.post(
        'https://auth.leboncoin.fr/api/authenticator/v1/users/login',
        auth_payload,
        { withCredentials: true }
    );
    const cookiesRaw = cookiesParser(response.headers['set-cookie'][0]);
    const cookies = { key: cookiesRaw[0][0], value: cookiesRaw[0][1] };
    let param = {};
    let responseToken = {};
    try {
        authReponse = await axios.get(
            response.data.redirect_uri,
            {
                headers: { Cookie: cookies.key + '=' + cookies.value },
            },
            { redirect: 'manual', withCredentials: true }
        );
    } catch (error) {
        const searchParams = new URL(
            'https://fakeurl.com/' + error.response.request.path
        ).searchParams;
        param.code = searchParams.get('code');
        param.state = searchParams.get('state');
        param.redirect_uri = 'https://www.leboncoin.fr/oauth2callbacknext/';
        param.grant_type = 'authorization_code';
    }
    try {
        responseToken = await axios.post(
            'https://auth.leboncoin.fr/api/authorizer/v1/lbc/token',
            param,
            { withCredentials: true }
        );
        // console.log(Object.keys(responseToken.headers));
    } catch (error) {
        console.log(error);
    }
    responseToken.data.uuid = uuid;
    return responseToken.data;
};

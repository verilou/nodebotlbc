const multer = require('@koa/multer');
const path = require('path');

const storage = multer.diskStorage({
    destination: (req, file, cb) => {
        cb(null, path.join('public', 'ads'));
    },
    filename: (req, file, cb) => {
        const uniqueSuffix = Date.now() + '-' + Math.round(Math.random() * 1e9);
        cb(null, file.fieldname + '-' + uniqueSuffix);
    },
});

const upload = multer({
    storage,
    fileFilter: (req, { mimetype }, cb) => {
        if (['image/png', 'image/jpg', 'image/jpeg'].includes(mimetype)) {
            cb(null, true);
        } else {
            let error = new Error('Only .png, .jpg and .jpeg format allowed!');
            error.status = 416;
            return cb(error, false);
        }
    },
});

module.exports = upload;

'use strict';
const fs = require('fs');
const path = require('path');
var sql = fs.readFileSync(path.join(__dirname, 'sql', 'cp.sql')).toString();

module.exports = {
    up: async (queryInterface, Sequelize) => {
        /**
         * Add seed commands here.
         *
         * Example:
         * await queryInterface.bulkInsert('People', [{
         *   name: 'John Doe',
         *   isBetaMember: false
         * }], {});
         */
        return queryInterface.sequelize.query(sql);
    },

    down: async (queryInterface, Sequelize) => {
        /**
         * Add commands to revert seed here.
         *
         * Example:
         * await queryInterface.bulkDelete('People', null, {});
         */
        await queryInterface.bulkDelete('codes_postaux', null, {});
    },
};
